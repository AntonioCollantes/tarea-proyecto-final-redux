import firebase from 'firebase/compat';
import 'firebase/compat/auth'
import 'firebase/compat/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyDp4kdxmxODeasdVxYwJYsc0y2N_Dpsn1w',
  authDomain: 'restaurante-g-30.firebaseapp.com',
  projectId: 'restaurante-g-30',
  storageBucket: 'restaurante-g-30.appspot.com',
  messagingSenderId: '909251879303',
  appId: '1:909251879303:web:cc5637f56a16ed142d7322',
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export {db, firebase} // axios
