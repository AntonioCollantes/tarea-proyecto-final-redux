import React, { useEffect } from 'react';
import Tabla from '../../components/Tabla';

// import ProductsContext from '../../context/products/ProductsContext';
import { useDispatch, useSelector } from 'react-redux';
import { obtenerProductos } from '../../redux/actions/productos';

const Productos = () => {
  const dispatch = useDispatch();
  const { productos } = useSelector((state) => state);
  const { listaProductos, deleteOk } = productos;

  useEffect(() => {
    dispatch(obtenerProductos());
  }, []);

  useEffect(() => {
    if (deleteOk) {
      dispatch(obtenerProductos());
    }
  }, [deleteOk]);

  return (
    <div>
      <div>
        <h1 style={{ color: 'red'}}>Productos</h1>
        {listaProductos.length > 0 && <Tabla listaProductos={listaProductos} />}
      </div>
    </div>
  );
};

export default Productos;
