import React, { useEffect } from 'react';
import Formulario from '../../components/Formulario';
import { useDispatch, useSelector } from 'react-redux';
import { obtenerProducto, editarProducto } from '../../redux/actions/productos';

const EditarProducto = (props) => {
  const dispatch = useDispatch();
  const { productos } = useSelector((state) => state);
  const { producto, editOk } = productos;

  useEffect(() => {
    dispatch(obtenerProducto(props.match.params.id));
  }, []);

  useEffect(() => {
    if (editOk) {
      props.history.push('/productos');
    }
  }, [editOk]);

  const edicion = (producto) => {
    dispatch(editarProducto(producto));
  };

  const tieneProducto = Object.keys(producto).length;

  return (
    <div>
      <h1 style={{ color: 'yellow'}}>Editar Producto</h1>
      {tieneProducto > 0 && (
        <Formulario
          producto={producto}
          editar={true}
          editarProducto={edicion}
        />
      )}
    </div>
  );
};

export default EditarProducto;
