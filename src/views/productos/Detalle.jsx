import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { obtenerProducto } from '../../redux/actions/productos';
import VerDetalle from '../../components/Formulario/VerDetalle';

const Detalle = (props) => {

  const dispatch = useDispatch();
  const { productos } = useSelector((state) => state);
  const { producto } = productos;

  useEffect(() => {
    dispatch(obtenerProducto(props.match.params.id));
  }, []);

  const tieneProducto = Object.keys(producto).length;

  const regresar = () => {
    props.history.push('/productos');
  }

  return (
    <div>
      {tieneProducto > 0 && (
        <VerDetalle
          producto={producto}
          volver = {regresar}
        />
      )}
    </div>
  );
};

export default Detalle;
