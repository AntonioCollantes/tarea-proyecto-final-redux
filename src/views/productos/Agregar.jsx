import React, { useEffect } from 'react';
import Formulario from '../../components/Formulario';

// import ProductsContext from '../../context/products/ProductsContext';
import { useDispatch, useSelector } from 'react-redux';
import { agregarProducto } from '../../redux/actions/productos';

const AgregarProducto = (props) => {
  // const { addOk, agregarProducto } = useContext(ProductsContext);
  const dispatch = useDispatch();
  const { productos } = useSelector((state) => state);
  const { addOk } = productos;

  useEffect(() => {
    if (addOk) {
      props.history.push('/productos');
    }
  }, [addOk]);

  const nuevoProducto = (producto) => {
    dispatch(agregarProducto(producto));
  };

  return (
    <div>
      <h1>Agregar Producto</h1>
      <Formulario agregarProductoNuevo={nuevoProducto} />
    </div>
  );
};

export default AgregarProducto;
