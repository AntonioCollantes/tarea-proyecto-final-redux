import { firebase } from '../../firebase/firebaseConfig';

export const iniciarSesion = (email, password) => {
  return async (dispatch) => {
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      dispatch(login(user.uid, user.email));
    } catch (error) {
      console.log(error);
    }
  };
};

const login = (uid, email) => {
  return {
    type: 'LOGIN',
    payload: { email: email, uid: uid },
  };
};

export const cerrarSesion = () => {
  return async (dispatch) => {
    try {
      await firebase.auth().signOut();

      dispatch(logout());
    } catch (error) {}
  };
};

const logout = () => {
  return {
    type: 'LOGOUT',
  };
};
