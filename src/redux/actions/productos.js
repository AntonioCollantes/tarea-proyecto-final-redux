import { db } from '../../firebase/firebaseConfig';
export const agregarProducto = (producto) => {
  return async (dispatch) => {
    try {
      await db.collection('productos').add(producto);
      dispatch({
        type: 'AGREGAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };
};

export const obtenerProductos = () => {
  return async (dispatch) => {
    try {
      const productos = [];
      const info = await db.collection('productos').get();
      info.forEach((item) => {
        productos.push({
          id: item.id,
          ...item.data(),
        });
      });

      dispatch({
        type: 'LLENAR_PRODUCTOS',
        payload: productos,
      });
    } catch (error) {
      console.log(error);
    }
  };
};

export const obtenerProducto = (id) => {
  return async (dispatch) => {
    try {
      const info = await db.collection('productos').doc(id).get();
      let producto = {
        id: id,
        ...info.data(),
      };
      dispatch({
        type: 'OBTENER_PRODUCTO',
        payload: producto,
      });
    } catch (error) {
      console.log(error);
    }
  };
};

export const editarProducto = (producto) => {
  return async (dispatch) => {
    try {
      const productoUpdate = { ...producto };
      delete productoUpdate.id;

      await db.collection('productos').doc(producto.id).update(productoUpdate);
      dispatch({
        type: 'EDITAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };
};

export const eliminarProducto = (id) => {
  return async (dispatch) => {
    try {
      await db.collection('productos').doc(id).delete();
      dispatch({
        type: 'ELIMINAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
