const initialState = {
  listaProductos: [],
  producto: {},
  addOk: false,
  editOk: false,
  deleteOk: false,
};
export const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'AGREGAR_PRODUCTO':
      return {
        ...state,
        addOk: action.payload,
      };
    case 'LLENAR_PRODUCTOS':
      return {
        ...state,
        addOk: false,
        editOk: false,
        listaProductos: action.payload,
        producto: {},
      };
    case 'OBTENER_PRODUCTO':
      return {
        ...state,
        producto: action.payload,
      };
    case 'EDITAR_PRODUCTO':
      return {
        ...state,
        editOk: action.payload,
      };
    case 'ELIMINAR_PRODUCTO':
      return {
        ...state,
        deleteOk: action.payload,
      };

    default:
      return state;
  }
};

export default productReducer;
