import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Header from '../components/Header';

const RutaPrivada = ({ estasAutenticado, component: Component, ...rest }) => {
  return (
    <>
      <Header />
      <main className='container'>
        <Route
          {...rest}
          component={(props) =>
            estasAutenticado ? <Component {...props} /> : <Redirect to='/' />
          }
        />
      </main>
    </>
  );
};

export default RutaPrivada;
