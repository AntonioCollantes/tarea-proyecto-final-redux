import React, { useState, useEffect } from 'react';
import './styles.css'

const initialDataForm = {
    nombre: '',
    precio: '',
    categoria: '',
    descripcion: '',
  };

const VerDetalle = ({ producto, volver }) => {

    const [dataForm, setDataForm] = useState(initialDataForm);

    useEffect(() => {
        setDataForm({ ...producto });
    }, []);

    return (
        <div id="registration-form">
            <div className="image"></div>
            <div className="frm">
                <h1>Detalle Producto</h1>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Nombre:</label>
                        <input type="text" className="form-control" value={dataForm.nombre} readOnly/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="precio">Precio:</label>
                        <input type="text" className="form-control" value={dataForm.precio} readOnly/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="categoria">Cetegoria:</label>
                        <input type="text" className="form-control" value={dataForm.categoria} readOnly/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="descripcion">Descripción:</label>
                        <textarea type="text" className="form-control" value={dataForm.descripcion} readOnly></textarea>
                    </div>
                    <br />
                    <div className="form-group">
                        <button type="submit" onClick={volver} className="btn btn-success btn-lg">regresar</button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default VerDetalle;