import React from 'react';
import RowTabla from '../RowTabla';

const Tabla = ({ listaProductos }) => {
  return (
    <table className='table'>
      <thead className='thead-dark'>
        <tr>
          <th scope='col' style={{ color: 'yellow'}}>#</th>
          <th scope='col' style={{ color: 'yellow'}}>Nombre</th>
          <th scope='col' style={{ color: 'yellow'}}>Categoria</th>
          <th scope='col' style={{ color: 'yellow'}}>Precio</th>
          <th scope='col' style={{ color: 'yellow'}}>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {listaProductos.map((item, index) => (
          <RowTabla key={item.id} info={item} indice={index} />
        ))}
      </tbody>
    </table>
  );
};

export default Tabla;
