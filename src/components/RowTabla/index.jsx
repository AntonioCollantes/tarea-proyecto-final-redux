import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { eliminarProducto } from '../../redux/actions/productos';

const RowTabla = ({ info, indice }) => {
  const { id, nombre, precio, categoria } = info;
  const dispatch = useDispatch();

  return (
    <tr>
      <th scope='row' style={{ color: 'red'}}>{indice + 1}</th>
      <td style={{ color: 'white'}}>{nombre}</td>
      <td style={{ color: 'white'}}>{categoria}</td>
      <td style={{ color: 'white'}}>S/ {precio}</td>
      <td className='d-flex justify-content-around'>
        <Link className='btn btn-warning' to={`/producto/editar/${id}`}>
          Editar
        </Link>
        <Link className='btn btn-primary' to={`/producto/detalle/${id}`}>
        Ver detalle
        </Link>
        <button
          className='btn btn-danger'
          onClick={() => dispatch(eliminarProducto(id))}
        >
          Eliminar
        </button>
      </td>
    </tr>
  );
};

export default RowTabla;
