import React, { useReducer } from 'react';

import AuthContext from './AuthContext';
import AuthReducer from './AuthReducer';

import { firebase } from '../../firebase/firebaseConfig';

const AuthState = ({ children }) => {
  const initialState = {
    email: null,
    uid: null,
  };

  const iniciarSesion = async (email, password) => {
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      dispatch(login(user.uid, user.email));
    } catch (error) {
      console.log(error);
    }
  };

  const login = (uid, email) => {
    return {
      type: 'LOGIN',
      payload: { email: email, uid: uid },
    };
  };

  const cerrarSesion = async () => {
    try {
      await firebase.auth().signOut();

      dispatch(logout());
    } catch (error) {}
  };

  const logout = () => {
    return {
      type: 'LOGOUT',
    };
  };

  const [state, dispatch] = useReducer(AuthReducer, initialState);

  return (
    <AuthContext.Provider value={{ iniciarSesion, cerrarSesion }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthState;
